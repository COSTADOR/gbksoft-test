/**
 * Style Guide
 * @link https://github.com/johnpapa/angular-styleguide
 */

module.exports = AppProvider;

AppProvider.$inject = [];

function AppProvider () {
  let provider = this;

  provider.config = {};
  provider.template = template;
  provider.$get = App;

  App.$inject = [
    '$state'
  ];

  function App () {
    let app = {
      template
    };

    Object.defineProperties(app, {
      config: {
        get () {
          return provider.config;
        }
      }
    });

    return app;
  }

  /**
     * Get template path
     *
     * @param {string} path
     * @returns {string}
     */
  function template (path) {
    return 'templates/' + path + '.html';
  }
}
