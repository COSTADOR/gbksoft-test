module.exports = Loading;

Loading.$inject = ['$http', '$timeout'];

function Loading ($http, $timeout) {
  return {
    restrict: 'A',
    link (scope, elm) {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, (loading) => {
        if (loading) {
          elm.show();
        } else {
          $timeout( () => {
            elm.hide();
          }, 200 );
        }
      });
    }
  };
}
