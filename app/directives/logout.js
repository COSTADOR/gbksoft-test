module.exports = Logout;

Logout.$inject = ['AuthService'];

function Logout(AuthService) {
  function LogoutController($state) {
    this.logout = function () {
      AuthService.logout();
      $state.go('login');
    };
  }

  LogoutController.$inject = ['$state'];

  return {
    restrict: 'E',
    template(elem) {
      return `<button ng-click="$ctrl.logout()" class="btn btn-danger">
              ${elem.html().trim() ? elem.html() : 'Logout'}
              </button>`;
    },
    scope: false,
    bindToController: true,
    controller: LogoutController,
    controllerAs: '$ctrl',
    compile: function compile(tElement, tAttrs, transclude) {
      tElement[!AuthService.isAuthenticated() ? 'hide' : 'show']();
    }
  };
}
