module.exports = List;

List.$inject = [
  'AuthService'
];

function List (AuthService) {
  let $ctrl = this;

  $ctrl.isAuthenticated = AuthService.isAuthenticated;
}
