module.exports = List;

List.$inject = [
  'UserService'
];

function List (UserService) {
  let $ctrl = this;

  $ctrl.users = [];
  $ctrl.initParams = {
    page: 1,
    'per-page': 20
  };
  $ctrl.currentParams = Object.assign({}, $ctrl.initParams);

  $ctrl.getUsers = getUsers;
  $ctrl.calcPageCount = calcPageCount;

  $ctrl.getUsers($ctrl.params);

  function getUsers (newParams) {
    $ctrl.currentParams = Object.assign({}, $ctrl.currentParams, newParams);
    return UserService.searchUsersBy($ctrl.currentParams).$promise.then((data) => {
      $ctrl.users = data;

      let totalCount = 1957; //until we don't have data from API

      $ctrl.pageCount = $ctrl.calcPageCount(totalCount, $ctrl.currentParams['per-page']);

      return $ctrl.users;
    });
  }

  function calcPageCount (totalCount, perPage) {
    return Math.ceil(totalCount / perPage);
  }
}
