module.exports = Map;

Map.$inject = [
  'UserService',
  'GMapService',
  'GeoLocationService',
  'MapCtrlHelper'
];

function Map (UserService, GMapService, GeoLocationService, MapCtrlHelper) {
  let $ctrl = this;

  $ctrl.gMap = null;
  $ctrl.users = [];
  $ctrl.currentParams = {};

  $ctrl.initMap = initMap;
  $ctrl.addListeners = addListeners;
  $ctrl.getUsers = getUsers;

  GeoLocationService.getCurrentUserPosition()
    .then(({coords}) => {
      console.log(coords);
      $ctrl.currentParams.lat = parseFloat(coords.latitude);
      $ctrl.currentParams.lon = parseFloat(coords.longitude);
      $ctrl.initMap();
      $ctrl.addListeners();
      $ctrl.getUsers();
    })
    .catch((err) => {
      console.error(err.message);
    });

  function initMap () {
    $ctrl.gMap = GMapService.createMap('user-google-map', {
      zoom: 10,
      center: {
        lat: $ctrl.currentParams.lat,
        lng: $ctrl.currentParams.lon
      }
    });
  }

  function addListeners () {
    $ctrl.gMap.map.addListener('idle', () => {
      let newCenter = $ctrl.gMap.map.getCenter();
      $ctrl.currentParams.lat = newCenter.lat();
      $ctrl.currentParams.lon = newCenter.lng();
      $ctrl.currentParams.radius = $ctrl.gMap.getVisibleRadius();
      $ctrl.getUsers();
    });
  }

  function getUsers (newParams) {
    $ctrl.currentParams = Object.assign($ctrl.currentParams, newParams);
    return UserService.searchUsersBy($ctrl.currentParams).$promise.then((data) => {
      $ctrl.users = data;

      let arrayForMarkers = MapCtrlHelper.prepareArrayForMarkers(data);

      $ctrl.gMap.createMarkers(arrayForMarkers);

      return $ctrl.users;
    });
  }
}
