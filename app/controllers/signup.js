module.exports = Signup;

Signup.$inject = [
  '$state',
  'AuthService',
  'NotificationService'
];

function Signup ($state, AuthService, NotificationService) {
  let $ctrl = this;

  $ctrl.credentials = {
    username: '',
    email: '',
    password: ''
  };

  $ctrl.signup = signup;

  function signup () {
    AuthService.signup($ctrl.credentials)
      .then(() => {
        NotificationService.showNotification('info', 'Success', 'User registered', 3000);
        $state.go('login');
      });
  }
}
