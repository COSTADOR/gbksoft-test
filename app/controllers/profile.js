module.exports = Profile;

Profile.$inject = [
  '$scope',
  'ProfileModel',
  'FileUploader',
  'UserService',
  '$rootScope',
  '$auth',
  'NotificationService',
  'GeoLocationService'
];

function Profile ($scope, ProfileModel, FileUploader, UserService,
  $rootScope, $auth, NotificationService, GeoLocationService) {
  let $ctrl = this;

  $ctrl.user = [];
  $ctrl.editMode = false;
  $ctrl.uploader = new FileUploader({
    url: $rootScope.config.apiUrl + 'user/profile/image',
    alias: 'image',
    autoUpload: true,
    headers: {
      Authorization: 'Bearer ' + $auth.getToken()
    }
  });
  $ctrl.uploader.onSuccessItem = function () {
    $ctrl.update();
  };

  $ctrl.cancel = cancel;
  $ctrl.save = save;
  $ctrl.setCurrentPosition = setCurrentPosition;
  $ctrl.deleteProfileImage = deleteProfileImage;
  $ctrl.toggleEditMode = toggleEditMode;
  $ctrl.getData = getData;
  $ctrl.update = update;

  $ctrl.getData();

  function getData () {
    return UserService.getCurrentUser().$promise.then((data) => {
      $ctrl.user = ProfileModel.$build(data);
      $ctrl.backupUser = data;
      return $ctrl.user;
    });
  }

  function update () {
    $ctrl.getData();
  }

  function cancel () {
    $ctrl.toggleEditMode(false);
    $ctrl.user = angular.copy($ctrl.backupUser);
  }

  function save () {
    $ctrl.user.$save().$then((data) => {
      $ctrl.user = ProfileModel.$build(data);
      $ctrl.backupUser = data;
      $ctrl.toggleEditMode(false);
      NotificationService.showNotification('info', 'Success', 'User data saved', 3000);
    });
  }

  function deleteProfileImage () {
    return UserService.deleteProfileImage().then(() => {
      $ctrl.update();
    });
  }

  function setCurrentPosition () {
    GeoLocationService.getCurrentUserPosition()
      .then(({coords}) => {
        $scope.$apply(() => {
          $ctrl.user.lat = coords.latitude.toString();
          $ctrl.user.lon = coords.longitude.toString();
        });
      })
      .catch((err) => {
        console.error(err.message);
      });
  }

  function toggleEditMode (mode = !this.editMode) {
    this.editMode = mode;
  }
}
