module.exports = User;

User.$inject = [
  '$state',
  'UserService'
];

function User ($state, UserService) {
  let $ctrl = this;

  UserService.getUserById($state.params.userId).$promise.then((data) => {
    $ctrl.user = data;
  });
}
