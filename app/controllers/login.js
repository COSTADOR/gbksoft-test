module.exports = Login;

Login.$inject = [
  '$state',
  'AuthService',
  '$auth',
  'NotificationService'
];

function Login ($state, AuthService, $auth, NotificationService) {
  let $ctrl = this;

  $ctrl.credentials = {
    username: '',
    password: ''
  };

  $ctrl.facebookCredentials = {
    code: ''
  };

  $ctrl.login = login;
  $ctrl.getFacebookCode = getFacebookCode;

  function getFacebookCode () {
    AuthService.authenticate('facebook').then((response) => {
      console.log(response); //until we can't get code from facebook
    });
  }

  function login (social) {
    if (social) {
      AuthService.fbLogin($ctrl.facebookCredentials)
        .then((response) => {
          loginSuccess(response.data.result.accessToken.token);
        });
    } else {
      AuthService.login($ctrl.credentials)
        .then((response) => {
          loginSuccess(response.data.result.token);
        });
    }
  }

  function loginSuccess (token) {
    //show success popup in toaster
    NotificationService.showNotification('info', 'Login', 'Success', 3000);
    $auth.setToken(token);
    $state.go('profile');
  }
}
