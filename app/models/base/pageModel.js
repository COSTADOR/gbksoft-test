module.exports = PagedModel;

PagedModel.$inject = ['restmod'];

function PagedModel (restmod) {
  return restmod.mixin(function () {
    this.setProperty('pageHeader', 'X-Pagination-Current-Page')
      .setProperty('pageCountHeader', 'X-Pagination-Page-Count')
      .setProperty('perPageHeader', 'X-Pagination-Per-Page')
      .setProperty('totalCountHeader', 'X-Pagination-Total-Count')
      .on('after-fetch-many', function (_response) {
        let page = _response.headers(this.$type.getProperty('pageHeader')),
          pageCount = _response.headers(this.$type.getProperty('pageCountHeader')),
          perPage = _response.headers(this.$type.getProperty('perPageHeader')),
          totalCount = _response.headers(this.$type.getProperty('totalCountHeader'));

        this.$page = (page !== undefined ? parseInt(page, 10) : 1);
        this.$pageCount = (pageCount !== undefined ? parseInt(pageCount, 10) : 1);
        this.$perPage = (perPage !== undefined ? parseInt(perPage, 10) : 20);
        this.$totalCount = (totalCount !== undefined ? parseInt(totalCount, 10) : 0);
      });
  });
}
