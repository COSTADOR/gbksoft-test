module.exports = BaseModel;

BaseModel.$inject = ['restmod'];

function BaseModel (restmod) {
  return restmod.mixin({
    $extend: {
      Model: {},
      Record: {},
      Collection: {},
      List: {}
    }
  });
}
