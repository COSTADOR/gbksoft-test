module.exports = ProfileModel;

ProfileModel.$inject = ['restmod'];

function ProfileModel (restmod) {
  return restmod.model('user/profile').mix({
    user: {
      belongsTo: 'UserModel'
    },
    $extend: {
      Model: {},
      Record: {}
    }
  });
}
