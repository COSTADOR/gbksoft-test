module.exports = UserModel;

UserModel.$inject = ['restmod'];

function UserModel (restmod) {
  return restmod.model('user', 'PagedModel').mix({
    name: {
      init: 'Guest'
    },
    viewCount: { ignore: 'R' },
    $extend: {
      Model: {},
      Record: {}
    }
  });
}
