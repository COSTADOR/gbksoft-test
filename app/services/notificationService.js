module.exports = notificationService;

notificationService.$inject = ['toaster'];

function notificationService (toaster) {
  return {
    showNotification
  };

  function showNotification (type, title, body, timeout) {
    toaster.pop({
      type,
      title,
      body,
      timeout
    });
  }
}
