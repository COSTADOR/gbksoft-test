module.exports = GeoLocationService;

function GeoLocationService () {
  return {
    getCurrentUserPosition,
    getDistanceInKm
  };

  function getCurrentUserPosition (options) {
    return new Promise(((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    }));
  }

  function getDistanceInKm (lon1, lat1, lon2, lat2) {
    let R = 6371; // Radius of the earth in km
    let dLat = toRad(lat2 - lat1);
    let dLon = toRad(lon2 - lon1);
    let a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2);
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = R * c; // Distance in km
    return d;
  }

  function toRad (Value) {
    return Value * Math.PI / 180;
  }
}
