module.exports = GMapService;

function GMapService () {
  class Map {
    constructor (id, options) {
      this.map = new google.maps.Map(document.getElementById(id), options);
      this.markers = [];
    }

    createMarkers (data) {
      this.deleteMarkers();

      if (this.markerCluster) this.markerCluster.clearMarkers();

      let labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

      for (let i = 0; i < data.length; i++) {
        let markerData = data[i];
        let {name, url, lon, lat} = markerData;
        if (lat && lon) {
          let marker = this.createMarker({
            title: name,
            url,
            position: {
              lat: parseFloat(lat),
              lng: parseFloat(lon)
            },
            label: labels[i % labels.length],
            map: this.map
          });
          this.markers.push(marker);
        }
      }

      this.markerCluster = new MarkerClusterer(this.map, this.markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
    }

    getVisibleRadius () {
      let center = this.map.getCenter();
      let bounds = this.map.getBounds();
      let start = bounds.getNorthEast();
      let end = bounds.getSouthWest();
      let distStart = google.maps.geometry.spherical.computeDistanceBetween(center, start) / 1000;
      let distEnd = google.maps.geometry.spherical.computeDistanceBetween(center, end) / 1000;

      return Math.max(distStart, distEnd) / 2;
    }

    deleteMarkers () {
      this.setMapOnAllMarkers(null);
      this.markers = [];
    }

    setMapOnAllMarkers (map) {
      for (let i = 0; i < this.markers.length; i++) {
        this.markers[i].setMap(map);
      }
    }

    createMarker (options) {
      let marker = new google.maps.Marker(options);

      google.maps.event.addListener(marker, 'click', function () {
        window.location.href = this.url;
      });

      return marker;
    }
  }

  return {
    createMap (id, options) {
      return new Map(id, options);
    }
  };
}
