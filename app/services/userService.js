module.exports = UserService;

UserService.$inject = ['UserModel', '$http', '$rootScope'];

function UserService (UserModel, $http, $rootScope) {
  return {
    getUsers,
    searchUsersBy,
    getUserById,
    getCurrentUser,
    deleteProfileImage,
    updateUserLocation
  };

  function getUserById (id) {
    return UserModel.$find(id).$then((data) => data);
  }

  function searchUsersBy (params = {}) {
    return UserModel.single('user/search').$fetch(params).$then((data) => data);
  }

  function getUsers () {
    return UserModel.single('user').$fetch().$then((data) => data);
  }

  function updateUserLocation (data, config) {
    return $http.put($rootScope.config.apiUrl + 'user/location', data, config)
      .then(
        (response) => response,
        (response) => response
      );
  }

  function getCurrentUser () {
    return UserModel.$find('current').$then((data) => data);
  }

  function deleteProfileImage (config) {
    return $http.delete($rootScope.config.apiUrl + 'user/profile/image', config)
      .then(
        (response) => response,
        (response) => response
      );
  }
}
