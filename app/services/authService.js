module.exports = AuthService;

AuthService.$inject = ['$state', '$auth', '$rootScope'];

function AuthService ($state, $auth, $rootScope) {
  this.isAuthenticated = function () {
    return $auth.isAuthenticated();
  };

  this.login = function (credentials) {
    return $auth.login(credentials);
  };

  this.fbLogin = function (credentials) {
    return $auth.login(credentials, {
      url: $rootScope.config.apiUrl + 'user/login/facebook'
    });
  };

  this.signup = function (credentials) {
    return $auth.signup(credentials);
  };

  this.authenticate = function (provider) {
    return $auth.authenticate(provider);
  };

  this.logout = function () {
    return $auth.logout();
  };

  this.requiresLogin = function (state) {
    return state.requiresLogin === true || state.requiresLogin === undefined;
  };

  this.checkAccess = function (toState) {
    if (this.isAuthenticated() && toState.name === 'login') {
      $state.go('home');

      return false;
    }

    if (this.requiresLogin(toState) && !this.isAuthenticated()) {
      $state.go('login', null, {location: true, reload: false});

      return false;
    }

    return true;
  };
}
