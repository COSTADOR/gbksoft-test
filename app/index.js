let config = process.env.CONFIG;

if (config.config.env === 'dev') {
  let req = require.context('../config/', false, /\.json$/);
  let localConfig = {};

  if (req.keys().indexOf('./config.local.json') > -1) {
    localConfig = req('./config.local.json');
  }

  angular.merge(
    config,
    req('./config.json'),
    localConfig
  );
}

let app = angular.module('app', config.app.module)
  .provider('App', require('./app'))
  .config(['AppProvider', function (AppProvider) {
    AppProvider.config = config.config;
  }])
  .run(['$rootScope', 'App', '$timeout', 'UserService', 'GeoLocationService', 'NotificationService',
    function ($rootScope, App, $timeout, UserService, GeoLocationService, NotificationService) {
      $timeout(() => {
        $rootScope.appReady = true;
      }, 500);
      $rootScope.App = App;
      $rootScope.config = config.config;

      setInterval(checkUserLocationChanges, 1000 * 60 * 60); //check one time per hour

      function checkUserLocationChanges () {
        UserService.getCurrentUser().$promise.then(({lat, lon}) => {
          let userLat = parseFloat(lat);
          let userLon = parseFloat(lon);

          GeoLocationService.getCurrentUserPosition()
            .then((position) => {
              let currentUserLon = position.coords.longitude;
              let currentUserLat = position.coords.latitude;

              let distanceInKm = GeoLocationService.getDistanceInKm(userLon, userLat, currentUserLon, currentUserLat);

              if (distanceInKm > 1) {
                UserService.updateUserLocation({
                  lat: currentUserLat.toString(),
                  lon: currentUserLon.toString()
                }).then(() => {
                  NotificationService.showNotification(
                    'info', 'Success', 'Actual user location saved to database', 3000
                  );
                });
              }
            });
        });
      }
    }]);

let type;
let name;

for (type in config.app) {
  if (!config.app.hasOwnProperty(type)) continue;

  let items = config.app[type];

  switch (type) {
    case 'controller':
      for (name in items) {
        app.controller(name, require('./controllers/' + items[name]));
      }
      break;

    case 'directive':
      for (name in items) {
        app.directive(name, require('./directives/' + items[name]));
      }
      break;

    case 'helpers':
      for (name in items) {
        app.service(name, require('./helpers/' + items[name]));
      }
      break;

    case 'service':
      for (name in items) {
        app.service(name, require('./services/' + items[name]));
      }
      break;

    case 'model':
      for (name in items) {
        app.factory(name, require('./models/' + items[name]));
      }
      break;

    case 'filter':
      for (name in items) {
        app.filter(name, require('./filters/' + items[name]));
      }
      break;

    case 'config':
      for (name = 0; name < items.length; name++) {
        app.config(require('./config/' + items[name]));
      }
      break;

    case 'run':
      for (name = 0; name < items.length; name++) {
        app.run(require('./config/' + items[name]));
      }
      break;
  }
}

angular.bootstrap(document, ['app'], {
  strictDi: true
});
