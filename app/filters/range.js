module.exports = Range;

function Range () {
  return function (input, total) {
    let count = parseInt(total, 10);
    for (let i = 0; i < count; i++) {
      input.push(i);
    }
    return input;
  };
}
