module.exports = run;

run.$inject = [
  '$rootScope',
  '$state',
  'AuthService'
];

function run ($rootScope, $state, AuthService) {
  $rootScope.$on('$stateChangeStart', (event, toState, toParams) => {
    if (toState.redirectTo) {
      event.preventDefault();
      $state.go(toState.redirectTo, toParams, {location: 'replace'});
    }
    if (!AuthService.checkAccess(toState)) {
      event.preventDefault();
    }
  });
}
