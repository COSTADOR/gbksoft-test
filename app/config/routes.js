module.exports = routes;

routes.$inject = [
  'AppProvider',
  '$stateProvider',
  '$urlRouterProvider'
];

function routes (AppProvider, $stateProvider, $urlRouterProvider) {
  $urlRouterProvider.otherwise(($injector) => {
    let $state = $injector.get('$state');

    $state.go('error', null, {location: false});
  });

  $stateProvider
    .state('app', {
      url: '',
      abstract: true,
      views: {
        '': {
          template: '<div ui-view noanimation="true"></div>'
        },
        'footer@': {
          templateUrl: AppProvider.template('views/footer')
        },
        'nav@': {
          templateUrl: AppProvider.template('views/nav'),
          controller: 'NavCtrl',
          controllerAs: '$ctrl'
        }
      }
    })

    .state('signup', {
      parent: 'app',
      url: '/signup',
      requiresLogin: false,
      views: {
        '': {
          templateUrl: AppProvider.template('views/signup'),
          controller: 'SignupCtrl',
          controllerAs: '$ctrl'
        }
      }
    })

    .state('login', {
      parent: 'app',
      url: '/login',
      requiresLogin: false,
      views: {
        '': {
          templateUrl: AppProvider.template('views/login'),
          controller: 'LoginCtrl',
          controllerAs: '$ctrl'
        }
      }
    })

    .state('profile', {
      parent: 'app',
      url: '/profile',
      views: {
        '': {
          templateUrl: AppProvider.template('views/profile'),
          controller: 'ProfileCtrl',
          controllerAs: '$profile'
        }
      }
    })

    .state('list', {
      parent: 'app',
      url: '/list',
      views: {
        '': {
          templateUrl: AppProvider.template('views/list'),
          controller: 'ListCtrl',
          controllerAs: '$ctrl'
        }
      }
    })

    .state('user', {
      parent: 'app',
      url: '/user/:userId',
      views: {
        '': {
          templateUrl: AppProvider.template('views/user'),
          controller: 'UserCtrl',
          controllerAs: '$ctrl'
        }
      }
    })

    .state('map', {
      parent: 'app',
      url: '/map',
      views: {
        '': {
          templateUrl: AppProvider.template('views/map'),
          controller: 'MapCtrl',
          controllerAs: '$ctrl'
        }
      }
    })

    .state('home', {
      parent: 'app',
      url: '/',
      redirectTo: 'profile'
    })

    .state('error', {
      parent: 'app',
      url: '/error',
      requiresLogin: false,
      views: {
        '': {
          templateUrl: AppProvider.template('views/error')
        },
        'nav@': {
          template: 'Error'
        }
      }
    });
}
