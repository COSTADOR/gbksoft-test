module.exports = HttpProviderConfig;

HttpProviderConfig.$inject = ['$httpProvider'];

function HttpProviderConfig ($httpProvider) {
  $httpProvider.interceptors.push(Interceptor);
}

Interceptor.$inject = ['$q', '$injector', 'NotificationService'];

function Interceptor ($q, $injector, NotificationService) {
  let errorsHandlers = {
    401: () => {
      $injector.get('$state').go('login', null, {location: false});
    },
    403: () => {
      $injector.get('$state').go('error', null, {location: false});
    }
  };

  return {
    request: (config) => config,
    responseError: (rejection) => {
      if (rejection && rejection.data && rejection.data.result) {
        rejection.data.result.forEach((result) => {
          //show errors in toaster popup
          NotificationService.showNotification('error', result.field, result.message, 3000);
        });
      }

      if (errorsHandlers[rejection.status]) {
        errorsHandlers[rejection.status](rejection);
      }

      return $q.reject(rejection);
    }
  };
}
