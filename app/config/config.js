module.exports = config;

config.$inject = [
  'AppProvider',
  '$httpProvider',
  '$compileProvider',
  '$locationProvider',
  '$authProvider',
  'restmodProvider'
];

function config (AppProvider, $httpProvider, $compileProvider, $locationProvider, $authProvider, restmodProvider) {
  //--------- $locationProvider ----------------------------------------------
  $locationProvider.html5Mode({enabled: true}).hashPrefix('!');

  //--------- $authProvider --------------------------------------------------
  $authProvider.baseUrl = AppProvider.config.apiUrl;
  $authProvider.tokenRoot = 'result';
  $authProvider.loginUrl = 'user/login';
  $authProvider.signupUrl = 'user/register';
  $authProvider.facebook({
    clientId: 1637129439866047,
    scope: ['public_profile', 'email'],
    redirectUri: AppProvider.config.apiUrl + 'auth/facebook'
  });

  //--------- $restmodProvider -----------------------------------------------
  restmodProvider
    .rebase('DefaultPacker', 'BaseModel', {
      $extend: {
        Model: {
          encodeUrlName: (_name) => _name.toLowerCase()
        }
      },
      $config: {
        style: 'GBKSOFT',
        urlPrefix: AppProvider.config.apiUrl,
        jsonRoot: 'result',
        primaryKey: 'id',
        jsonMeta: '_meta'
      }
    });

  //--------------------------------------------------------------------------
  $compileProvider.debugInfoEnabled(AppProvider.config.env === 'dev');
}
