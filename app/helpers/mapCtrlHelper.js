module.exports = MapCtrlHelper;

function MapCtrlHelper () {
  return {
    prepareArrayForMarkers
  };

  function prepareArrayForMarkers (data) {
    let arr = [];

    for (let key in data.$response.data.result) {
      if (data[key] && data[key].id) {
        data[key].url = window.location.origin + '/user/' + data[key].id;
        arr.push(data[key]);
      }
    }

    return arr;
  }
}
